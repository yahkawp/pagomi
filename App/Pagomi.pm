package App::Pagomi;

use warnings;
use strict;
use Data::Dumper;

our $Lang = 'ml';

my %syn = (
    matchstart => {
        ml => '',
    },
    matchend => {
        ml => ''
    },
);



my %ppairs = qw| ) ( ] [ ( ) [ ] |;



sub linetext {
    my ($curind, $words, $terminator) = @_;
    
    my $leadws = ' ' x $curind;
    $leadws ||= '';

    my ($neo, @nwords);
    if(ref $words->[0] eq 'ARRAY'){
        $neo = 1;
    }
    foreach (@$words){
        if(ref $_ eq 'ARRAY'){
            push @nwords, $_->[0];
        }else{
            push @nwords, $_;
        }

    }
    
    my ($single, @pars) = @$words;
    my @res = grep /\)/, @pars;


    if(@res > 0){
        if ($single eq ')'){
            return ('', [ $terminator , @nwords]  , undef );
        }else{

            if (@res == @pars){  # 3)))
                return ($leadws, [ $terminator, @nwords ], 1);
            }
        }
    }
    if(@$words == 0 ){
        return $leadws, [ '', @nwords ] , undef;
    }elsif(@$words == 1 ){
        return ($leadws, [ $terminator, @nwords ], 1);
    }else{
        return ($neo)
            ? ($leadws, [ $terminator, @nwords ], $neo)
            : ($leadws, [ $terminator , @nwords]  , undef );
    }
}
sub pars {
    my ($_curind, $_indstack, $_wasneo) = @_;

    my $cnt;
    my $rec; $rec = sub {
        my ($curind, $indstack, $wasneo) = @_;
        my ($prevind ) = $indstack->[$#$indstack] if @$indstack;
        $prevind ||= 0;
        my ($pprevind ) = $indstack->[$#$indstack-1] if (@$indstack> 1 );
        if ($prevind < $curind){

            push @$indstack, $curind;
            return ('(', '');
        }elsif ($prevind == $curind){
            return ($wasneo)
                ? ('', '')
                : ('(', ')');
        }else{
            pop @$indstack;
            my ($start, @ends);
            if(@$indstack > 0 ){
                ($start, @ends) = $rec->($curind, $indstack, undef) ;
            }else{
                ($start, @ends) = ();
                push @$indstack, $curind;
            }
            return ($wasneo)
                ? ('', '', @ends )
                : ('(', ')',  @ends );
        }
    };
    return  $rec->($_curind, $_indstack, $_wasneo);
}

sub mkstring {
    my $textbox = shift;

    my ($term, @words) = @$textbox;
    my $str = join ' ', @words;
    return  $str . $term;
}
sub getline {
    my ($init ) = @_;

    my ($prevws, $prevtext, $wasneo, $indstack) = ('', '', undef, []);
    my @empties = ();
    

    return sub { 
        my ($curind, $words, $terminator) = @_;
        

        if($init){ # first line
            
            (undef, $prevtext, $wasneo) = linetext(0, $words, $terminator);
            
            undef $init;
            return undef;
        }else{
            if ($words){

                if(@$words){
                    my ($start, @ends )= pars($curind , $indstack, $wasneo);
                    my ($curws, $curtext, $isneo) = linetext($curind, $words, $terminator);
                    $wasneo = $isneo;

                    my $endpars = join '',  @ends;
                    my $prevstring = mkstring $prevtext;
                    
                    my $returntext = ($prevstring)
                        ? $prevws . $start . $prevstring .  $endpars 
                        : '';
                    ($prevws, $prevtext) = ($curws, $curtext);
                    my @es = @empties;
                    undef @empties;
                    return [ $returntext, @es ] ;
                }else{
                    push @empties, '';
                    return undef;
                }
            }else{ # last line
                my $prevstring = mkstring $prevtext;
                push @$indstack, 1 unless $wasneo;
                my $size = @$indstack;
                my $terminal = ')' x $size;
                my (undef, @words ) = @$prevtext;
                my @es = @empties;
                undef @empties;
                if(@words > 1 ){
                    return ($wasneo)
                        ? [ $prevws .  '' . $prevstring .  $terminal, @es ]  
                        : [ $prevws .  '(' . $prevstring .  $terminal, @es ] ; 
                }else{
                    return [ $prevws .  $prevstring . $terminal, @es ] ; 
                }  
            }
        }
    };
}


sub run_pagomi {
    my ($lineref ) = @_;


    my (@lines, @pars) = ();
    my $linegetter = getline(1);
    my @words;
    foreach my $ln (@$lineref){
        my ($ind, $leadws, $curind, $terminator , $matchbar) = (undef, 1, 0, '', undef);

        undef @words ; # maybe futurure multiline stuff / strings ...

        my @chars = ($ln)
            ?  split '', $ln
            : ();
        

        my ($wrap, $prevch, $colons, @word, ) = (undef, undef, 0, ());
        foreach my $ch (@chars){
            if($wrap){
                if($wrap eq '"'){
                    if($ch eq '"'){
                        my $w = join '', @word; undef @word; undef $wrap;
                        push @words, '"' .  $w . '"' ;
                    }else{
                        push @word, $ch;
                    }
                }
            }else{
                if ($ch eq ' '){
                    if($leadws ){ 
                        $curind++; 
                    }else{
                        if(@word){
                            my $w = (join '', @word); undef @word;
                            if($w eq '/:'){
                                push @words, ['(lambda ('];
                                $terminator = ')';
                            }elsif($w eq '=>'){
                                push @words, $w unless $matchbar
                            }elsif($w eq '|'){
                                if(@words){
                                    unshift @words, $syn{matchstart}->{$Lang};
                                    push @words, ')(';
                                    $terminator = $syn{matchend}->{$Lang};
                                }else{
                                    $matchbar = 1;
                                    unshift @words, $syn{matchstart}->{$Lang};
                                    $terminator = $syn{matchend}->{$Lang};
                                }
                            }else{
                                push @words, $w;
                            }
                        }else{
                            if($prevch eq ':' ){
                                push @words, '(' ;
                                $terminator = ')';
                            }else{
                            }
                        }
                    }
                }else{
                    undef $leadws;
                    if($ch =~ /\"/ ){
                        $wrap = $ch ;
                    }elsif($ch =~ /\;/){
                        last
                    }elsif($ch =~ /\n/){
                    }elsif($ch =~ /\:/){
                        push @word, $ch if @word;
                    }elsif($ch =~ /\(|\[/ ){
                        push @pars, $ch; 
                        if(@word){  # neotonic   foo(g) -> (foo g)
                           my $w =   join '', @word; undef @word;
                           push @words, [join '', $ch . $w .  ' ' ];
                        }elsif($prevch eq ':'){
                            push @words, '(';
                            $terminator = ')';
                        }else{
                            push @words, $ch;
                        }
                    }elsif($ch =~ /\)|\]/ ){
                        my $p = pop @pars;
                        if(@word){ push @words, (join '', @word);  undef @word; }
                        push @words, $ch;
                        die "Err: par mismatch " unless $p eq $ppairs{$ch};
                    }else{
                        push @word, $ch;
                    }
                }
            }
            $prevch = $ch;
        }


        push @words, (join '', @word ) if @word ;

        #unless ($curind ){ $curind = 0 }
        my ($line) = $linegetter->($curind,  \@words, $terminator );
        push @lines, @$line if (defined $line); 
    }


    print "\n";

    my ($line) = $linegetter->(undef, undef, '');
    push @lines, @$line if (defined $line); 


    my $txt = join "\n", @lines if @lines ;
    $txt ||= '';
    print $txt;
    #print $txt . endpars(\@pars);
}

sub run_file {
    my ($file) = @_;
    open (my $fh, '<', $file);
    my @lns = <$fh> ;
    close $fh;
    return  run_pagomi(\@lns);
}
1;
